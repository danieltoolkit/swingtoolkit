package com.valley.model;

/**
 * Created by MuXin on 2018/10/8.
 */
public class IdCard {
    private String manSex = "男";
    private String manId;
    private String womanSex = "女";
    private String womanId;

    public String getManSex() {
        return manSex;
    }

    public void setManSex(String manSex) {
        this.manSex = manSex;
    }

    public String getManId() {
        return manId;
    }

    public void setManId(String manId) {
        this.manId = manId;
    }

    public String getWomanSex() {
        return womanSex;
    }

    public void setWomanSex(String womanSex) {
        this.womanSex = womanSex;
    }

    public String getWomanId() {
        return womanId;
    }

    public void setWomanId(String womanId) {
        this.womanId = womanId;
    }

    public IdCard(String manId, String womanId) {
        this.manId = manId;
        this.womanId = womanId;
    }

    public IdCard() {
    }
}
