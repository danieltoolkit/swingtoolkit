package com.valley.model;

import javax.swing.*;
import javax.swing.event.TableColumnModelListener;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;

/**
 * Created by MuXin on 2018/10/8.
 */
public class IdCardTableModel extends AbstractTableModel {
    private List<IdCard> idCardList = new ArrayList<>();


    public void setIdCardList(List<IdCard> idCardList) {
        this.idCardList = idCardList;
        this.fireTableDataChanged();// 同时通知JTabel数据对象更改, 重绘界面
    }

    /**
     * Returns the number of rows in the model. A
     * <code>JTable</code> uses this method to determine how many rows it
     * should display.  This method should be quick, as it
     * is called frequently during rendering.
     *
     * @return the number of rows in the model
     * @see #getColumnCount
     */
    @Override
    public int getRowCount() {
        return idCardList.size();
    }

    /**
     * Returns the number of columns in the model. A
     * <code>JTable</code> uses this method to determine how many columns it
     * should create and display by default.
     *
     * @return the number of columns in the model
     * @see #getRowCount
     */
    @Override
    public int getColumnCount() {
        return 4;
    }

    /**
     * Returns the value for the cell at <code>columnIndex</code> and
     * <code>rowIndex</code>.
     *
     * @param rowIndex    the row whose value is to be queried
     * @param columnIndex the column whose value is to be queried
     * @return the value Object at the specified cell
     */
    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        IdCard idCard = idCardList.get(rowIndex);
        if (columnIndex == 0) {
            return idCard.getManSex();
        } else if (columnIndex == 1) {
            return idCard.getManId();
        } else if (columnIndex == 2) {
            return idCard.getWomanSex();
        } else if (columnIndex == 3) {
            return idCard.getWomanId();
        }
        return null;

    }

    @Override
    public String getColumnName(int columnIndex) {
        String result = "";
        if (columnIndex == 0) {
            result = "性别";
        } else if (columnIndex == 1) {
            result = "证件号码";
        } else if (columnIndex == 2) {
            result = "性别";
        } else if (columnIndex == 3) {
            result = "证件号码";
        }
        return result;
    }

    @Override
    public boolean isCellEditable(int rowIndex, int columnIndex) {
        if (columnIndex == 1 || columnIndex == 3)
            return true;
        else
            return false;
    }
}
