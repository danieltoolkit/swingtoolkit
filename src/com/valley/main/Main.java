package com.valley.main;

import com.sun.deploy.util.StringUtils;
import com.valley.model.IdCard;
import com.valley.model.IdCardTableModel;
import com.valley.util.MD5Util;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import javax.swing.border.TitledBorder;
import javax.swing.event.TreeSelectionEvent;
import javax.swing.event.TreeSelectionListener;
import javax.swing.table.TableCellEditor;
import javax.swing.tree.DefaultMutableTreeNode;
import java.awt.*;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.StringSelection;
import java.awt.datatransfer.Transferable;
import java.awt.event.*;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.util.*;
import java.util.List;

/**
 * Created by MuXin on 2018/10/8.
 */
public class Main extends JFrame {
    private static int jFrameWidth = 1000;
    private static int jFrameHieght = 700;

    public static int getjFrameWidth() {
        return jFrameWidth;
    }

    public static void setjFrameWidth(int jFrameWidth) {
        Main.jFrameWidth = jFrameWidth;
    }

    public static int getjFrameHieght() {
        return jFrameHieght;
    }

    public static void setjFrameHieght(int jFrameHieght) {
        Main.jFrameHieght = jFrameHieght;
    }

    private static JPanel jPanel = null;
    private static Container container = null;

    public static void main(String[] args) {

        try {
            //设置窗口风格
            UIManager.setLookAndFeel(UIManager.getCrossPlatformLookAndFeelClassName());
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (UnsupportedLookAndFeelException e) {
            e.printStackTrace();
        }
        JFrame jFrame = new JFrame("牧心Space");//创建顶层容器并初始化
        container = jFrame.getContentPane();//获取面板容器
        jPanel = new JPanel();//创建面板panel并初始化
        jPanel.setLayout(new BorderLayout());//设置布局管理器FlowLayout
        jPanel.add(new NorthPanel(), BorderLayout.NORTH);//向容器中添加组件label
        jPanel.add(new WestPanel(), BorderLayout.WEST);//向容器中添加组件button
        jPanel.add(new IDGeneratorPanel(), BorderLayout.CENTER);
        container.add(jPanel);//将面板添加到窗口

        jFrame.addWindowListener(new WindowAdapter() {
            /**
             * Invoked when a window is in the process of being closed.
             * The close operation can be overridden at this point.
             *
             * @param e
             */
            @Override
            public void windowClosing(WindowEvent e) {
                System.exit(0);//程序退出
            }
        });


        jFrame.setSize(jFrameWidth, jFrameHieght);
        jFrame.setVisible(true);
        int x = (Toolkit.getDefaultToolkit().getScreenSize().width - jFrameWidth) / 2;
        int y = (Toolkit.getDefaultToolkit().getScreenSize().height - jFrameHieght) / 2;
        jFrame.setLocation(x, y);

    }

    public static void changeCenterPanel(String nodeName) {
        jPanel.remove(2);
        jPanel.repaint();
        if("身份证号码随机生成".equals(nodeName)){
            jPanel.add(new IDGeneratorPanel(), BorderLayout.CENTER);
        }else if("URL Decode and Encode".equals(nodeName)){
            jPanel.add(new URLDecoderPanel(), BorderLayout.CENTER);
        }else if("Md5加密".equals(nodeName)){
            jPanel.add(new MD5EncodePanel(),BorderLayout.CENTER);
        }
        jPanel.revalidate();
//        container.repaint();
//        container.removeAll();
//        container.add(jPanel);
    }
}

/**
 * 首部标题
 */
class NorthPanel extends JPanel {
    public NorthPanel() {
        BoxLayout layout = new BoxLayout(this, BoxLayout.X_AXIS);
        this.setLayout(layout);
        final JLabel jLabel = new JLabel("开发工具箱");
        jLabel.setFont(new Font("Dialog", 1, 26));
        jLabel.setForeground(Color.white);
        this.add(jLabel);
        this.setBackground(new Color(41, 101, 168));
        this.setBorder(new EmptyBorder(10, 10, 10, 10));
    }
}

class WestPanel extends JPanel {
    DefaultMutableTreeNode root;
    DefaultMutableTreeNode IDCreator;
    DefaultMutableTreeNode URLDecoder;
    DefaultMutableTreeNode Md5Encode;

    public WestPanel() {
        this.setLayout(new FlowLayout());
        // 创建节点
        root = new DefaultMutableTreeNode("工具箱");
        IDCreator = new DefaultMutableTreeNode("身份证号码随机生成");
        URLDecoder = new DefaultMutableTreeNode("URL Decode and Encode");
        Md5Encode = new DefaultMutableTreeNode("Md5加密");
        root.add(IDCreator);
        root.add(URLDecoder);
        root.add(Md5Encode);
        JTree jTree = new JTree(root);
        JScrollPane jScrollPane = new JScrollPane(jTree);
        jTree.setAutoscrolls(true);
        jTree.setBackground(new Color(234, 234, 234, 234));
        jTree.addTreeSelectionListener(new TreeSelectionListener() {
            @Override
            public void valueChanged(TreeSelectionEvent e) {
                DefaultMutableTreeNode node = (DefaultMutableTreeNode) jTree.getLastSelectedPathComponent();
                if (node == null) {
                    return;
                }
                if (node.isLeaf()) {
                    String nodeName = node.toString();
                    System.out.println("nodeName:" + node.toString());
                    Main.changeCenterPanel(nodeName);
                }


            }
        });
        jScrollPane.setBorder(new EmptyBorder(0, 0, 0, 0));
        this.add(jScrollPane);
        this.setBackground(new Color(234, 234, 234, 234));
    }
}

class IDGeneratorPanel extends JPanel implements ActionListener {
    private JComboBox provinceComboBos = null;
    private JComboBox cityComboBos = null;
    private JComboBox countyComboBos = null;

    public IDGeneratorPanel() {
        this.setLayout(new BorderLayout());
        JPanel titleJPanel = new JPanel();
        titleJPanel.setLayout(new BoxLayout(titleJPanel, BoxLayout.X_AXIS));
        final JLabel titleLable = new JLabel("身份证号码随机生成");
        titleLable.setForeground(Color.white);
        titleLable.setFont(new Font("Dialog", 1, 16));
        titleJPanel.add(titleLable);
        titleJPanel.setBackground(new Color(17, 134, 150));
        JPanel contentJPanel = new JPanel();

        contentJPanel.setBackground(Color.white);
//        contentJPanel.setLayout(new GridLayout(1, 3));
        JLabel provinceLable = new JLabel("省份：");
        JLabel cityLable = new JLabel("城市：");
        JLabel countyLable = new JLabel("区县：");
        JLabel manLable = new JLabel("对象：");
        JLabel numLable = new JLabel("数量：");

        String[] provinceArray = {"四川", "河南", "北京"};
        String[] cityArray = {"成都", "广元", "德阳"};
        String[] countyArray = {"锦江区", "高新区", "龙泉驿区"};
        provinceComboBos = new JComboBox(provinceArray);
        provinceComboBos.addActionListener(this);
        provinceComboBos.setSize(100, 30);
        cityComboBos = new JComboBox(cityArray);
        cityComboBos.setSize(100, 30);
        countyComboBos = new JComboBox(countyArray);
        countyComboBos.setSize(100, 30);

        JRadioButton manRadioButton = new JRadioButton("男方", true);
        JRadioButton womanRadioButton = new JRadioButton("女方", true);
        JTextField numTextField = new JTextField("1", 3);

        String[] columnNames =
                {"性别", "证件号码", "性别", "证件号码"};
        String[][] obj = new String[1][4];
        JTable jTable = new JTable();
        final IdCardTableModel idCardTableModel = new IdCardTableModel();
        jTable.setModel(idCardTableModel);
        jTable.getColumnModel().getColumn(0).setPreferredWidth(80);
        jTable.getColumnModel().getColumn(1).setPreferredWidth(200);
        jTable.getColumnModel().getColumn(2).setPreferredWidth(80);
        jTable.getColumnModel().getColumn(3).setPreferredWidth(200);
        jTable.addKeyListener(new KeyListener() {
            private int lastCode = 0;

            @Override
            public void keyTyped(KeyEvent e) {
                System.out.println(e.getKeyChar());
            }

            @Override
            public void keyPressed(KeyEvent e) {
                int code = e.getKeyCode();
                if (lastCode == 157 && code == 67) {
                    System.out.println(jTable.getValueAt(jTable.getSelectedRow(), jTable.getSelectedColumn()));
                    // 获取系统剪贴板
                    Clipboard clipboard = Toolkit.getDefaultToolkit().getSystemClipboard();
                    // 封装文本内容
                    Transferable trans = new StringSelection((String) jTable.getValueAt(jTable.getSelectedRow(),
                            jTable.getSelectedColumn()));
                    // 把文本内容设置到系统剪贴板
                    clipboard.setContents(trans, null);
                }
                lastCode = code;
            }

            @Override
            public void keyReleased(KeyEvent e) {
                lastCode = 0;
            }
        });
        JScrollPane scrollpane = new JScrollPane(jTable);

        JButton generateButton = new JButton("生成");
        generateButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String province = (String) provinceComboBos.getSelectedItem();
                String city = (String) cityComboBos.getSelectedItem();
                String county = (String) countyComboBos.getSelectedItem();
                boolean manSelect = manRadioButton.isSelected();
                boolean womanSelect = womanRadioButton.isSelected();
                String num = numTextField.getText();
                System.out.println(province + "-" + city + "-" + county + "-" + manSelect + "-"
                        + womanSelect + "-" + num);
                java.util.List<IdCard> idCardList = generateIdCard(province, city, county, manSelect, womanSelect, num);
                idCardTableModel
                        .setIdCardList(idCardList);
            }
        });


        contentJPanel.add(provinceLable);
        contentJPanel.add(provinceComboBos);
        contentJPanel.add(cityLable);
        contentJPanel.add(cityComboBos);
        contentJPanel.add(countyLable);
        contentJPanel.add(countyComboBos);
        contentJPanel.add(manLable);
        contentJPanel.add(manRadioButton);
        contentJPanel.add(womanRadioButton);
        contentJPanel.add(numLable);
        contentJPanel.add(numTextField);
        contentJPanel.add(generateButton);
        contentJPanel.add(scrollpane);


        this.add(titleJPanel, BorderLayout.NORTH);
        this.add(contentJPanel, BorderLayout.CENTER);

    }

    private List<IdCard> generateIdCard(String province, String city, String county, boolean manSelect, boolean
            womanSelect,
                                        String num) {
        List<IdCard> idCardList = new ArrayList<>();
        if (num == null || "".equals(num.trim())) {
            num = "1";
        }
        int numInt = 1;
        try {
            numInt = Integer.parseInt(num);
        } catch (Exception e) {
        }
        switch (province) {
            case "四川":
                province = "51";
                break;
            case "北京":
                province = "11";
                break;
            case "河南":
                province = "41";
                break;
        }
        switch (city) {
            case "成都":
                city = "01";
                break;
            case "广元":
                city = "08";
                break;
            case "德阳":
                city = "06";
                break;
            case "北京":
                city = "01";
                break;
            case "信阳":
                city = "03";
                break;
        }
        switch (county) {
            case "高新区":
                county = "99";
                break;
            case "锦江区":
                county = "04";
                break;
            case "龙泉驿区":
                county = "12";
                break;
            case "海淀区":
                county = "08";
                break;
            case "固始县":
                county = "26";
                break;
        }

        for (int i = 0; i < numInt; i++) {
            IdCard idCard = new IdCard();
            if (manSelect == true) {
                idCard.setManId(getRandomIdCard(province + city + county, 1));
            }
            if (womanSelect == true) {
                idCard.setWomanId(getRandomIdCard(province + city + county, 2));
            }
            idCardList.add(idCard);

        }
        return idCardList;
    }

    private String getRandomIdCard(String regionCode, int sex) {
        String idCard = "";
        int nowYear = 2018;
        long randomBirth = 1921l + (long) (Math.random() * (nowYear - 1921));
        Random random = new Random(new Date().getTime());
        int randInt = 10 + (random.nextInt(89));

        int[] IDnums = new int[17];
        int[] coe = {7, 9, 10, 5, 8, 4, 2, 1, 6, 3, 7, 9, 10, 5, 8, 4, 2};    //1-17位对应的系数
        idCard = regionCode + randomBirth + "1001" + randInt + (sex == 1 ? "1" : "2");
        System.out.println(idCard + "-" + randomBirth);
        for (int i = 0; i < idCard.length(); i++) {
            IDnums[i] = idCard.charAt(i) - '0';
        }
        int sum = getSum(IDnums, coe);
        int last = getLastNum(sum);
        if (last == 10) {
            idCard += "X";
        } else {
            idCard += last;
        }

        return idCard;
    }

    public static int getSum(int IDnums[], int coe[]) {
        int sum = 0;
        for (int i = 0; i < 17; i++) {
            sum += IDnums[i] * coe[i];
        }
        return sum;
    }

    public static int getLastNum(int num) {
        num = num % 11;
        switch (num) {
            case 0:
                num = 1;
                break;
            case 1:
                num = 0;
                break;
            case 2:
                num = 10;
                break;
            case 3:
                num = 9;
                break;
            case 4:
                num = 8;
                break;
            case 5:
                num = 7;
                break;
            case 6:
                num = 6;
                break;
            case 7:
                num = 5;
                break;
            case 8:
                num = 4;
                break;
            case 9:
                num = 3;
                break;
            case 10:
                num = 2;
                break;
        }
        return num;


    }

    /**
     * Invoked when an action occurs.
     *
     * @param e
     */
    @Override
    public void actionPerformed(ActionEvent e) {
        String itemSize = (String) provinceComboBos.getSelectedItem();

        System.out.println(itemSize);
    }
}

class URLDecoderPanel extends JPanel{
    public URLDecoderPanel(){
        this.setLayout(new BorderLayout());
        JPanel titleJPanel = new JPanel();
        titleJPanel.setLayout(new BoxLayout(titleJPanel, BoxLayout.X_AXIS));
        final JLabel titleLable = new JLabel("URL Decode and Encode");
        titleLable.setForeground(Color.white);
        titleLable.setFont(new Font("Dialog", 1, 16));
        titleJPanel.add(titleLable);
        titleJPanel.setBackground(new Color(17, 134, 150));
        JPanel contentJPanel = new JPanel();
        contentJPanel.setBackground(Color.white);
        contentJPanel.setLayout(new FlowLayout());
        JTextArea sourceTextArea = new JTextArea(6,60);
        sourceTextArea.setLineWrap(true);        //激活自动换行功能
        sourceTextArea.setWrapStyleWord(true);            // 激活断行不断字功能
        sourceTextArea.setBorder(new TitledBorder("URL编码字符串"));
        JTextArea decodeTextArea = new JTextArea(6,60);
        decodeTextArea.setBorder(new TitledBorder("URL解码字符串"));
        decodeTextArea.setLineWrap(true);        //激活自动换行功能
        decodeTextArea.setWrapStyleWord(true);            // 激活断行不断字功能
        JButton decodeButton = new JButton("解码");
        decodeButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String source = sourceTextArea.getText();
                if (source == null || "".equals(source.trim())){
                    return;
                }
                try {
                    decodeTextArea.setText(URLDecoder.decode(source,"utf-8"));
                } catch (UnsupportedEncodingException e1) {
                    e1.printStackTrace();
                }
            }
        });
        JButton encodeButton = new JButton("编码");
        encodeButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String decoderStr = decodeTextArea.getText();
                if (decoderStr == null || "".equals(decoderStr.trim())){
                    return;
                }
                try {
                    sourceTextArea.setText(URLEncoder.encode(decoderStr,"utf-8"));
                } catch (UnsupportedEncodingException e1) {
                    e1.printStackTrace();
                }
            }
        });
        contentJPanel.add(sourceTextArea);
        JPanel buttonPanel = new JPanel();
        buttonPanel.add(decodeButton);
        buttonPanel.add(encodeButton);
        contentJPanel.add(buttonPanel);
        contentJPanel.add(decodeTextArea);


        this.add(titleJPanel, BorderLayout.NORTH);
        this.add(contentJPanel, BorderLayout.CENTER);

    }
}

class MD5EncodePanel extends JPanel{
    public MD5EncodePanel(){
        this.setLayout(new BorderLayout());
        JPanel titleJPanel = new JPanel();
        titleJPanel.setLayout(new BoxLayout(titleJPanel, BoxLayout.X_AXIS));
        final JLabel titleLable = new JLabel("MD5 Encode");
        titleLable.setForeground(Color.white);
        titleLable.setFont(new Font("Dialog", 1, 16));
        titleJPanel.add(titleLable);
        titleJPanel.setBackground(new Color(17, 134, 150));
        JPanel contentJPanel = new JPanel();
        contentJPanel.setBackground(Color.white);
        contentJPanel.setLayout(new FlowLayout());
        JTextArea sourceTextArea = new JTextArea(6,60);
        sourceTextArea.setLineWrap(true);        //激活自动换行功能
        sourceTextArea.setWrapStyleWord(true);            // 激活断行不断字功能
        sourceTextArea.setBorder(new TitledBorder("源字符串"));

        JTextField decodeTextArea = new JTextField(60);
        decodeTextArea.setBorder(new TitledBorder("32位小写MD5"));
        JTextField decodeTextArea1 = new JTextField(60);
        decodeTextArea1.setBorder(new TitledBorder("32位大写MD5"));
        JButton decodeButton = new JButton("加密");
        decodeButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String source = sourceTextArea.getText();
                if (source == null || "".equals(source.trim())){
                    return;
                }
                try {
                    decodeTextArea.setText(MD5Util.md5(source));
                    decodeTextArea1.setText(MD5Util.md5(source).toUpperCase());
                } catch (Exception e1) {
                    e1.printStackTrace();
                }
            }
        });
        contentJPanel.add(sourceTextArea);
        JPanel buttonPanel = new JPanel();
        buttonPanel.add(decodeButton);
        contentJPanel.add(buttonPanel);
        contentJPanel.add(decodeTextArea);
        contentJPanel.add(decodeTextArea1);


        this.add(titleJPanel, BorderLayout.NORTH);
        this.add(contentJPanel, BorderLayout.CENTER);

    }
}


 





